# Maintainer: ObserverOfTime <chronobserver@disroot.org>
# Based on aria2-git

pkgname=aria2-openssl-git
pkgver=1.37.0.r10.ga8cb271b0
pkgrel=1
pkgdesc="Download utility that supports HTTP(S), \
FTP, BitTorrent, and Metalink - built with OpenSSL"
arch=('x86_64')
url='https://aria2.github.io/'
license=('GPL')
depends=(
  'bash-completion'
  'c-ares'
  'ca-certificates'
  'jemalloc'
  'libssh2'
  'libuv'
  'libxml2'
  'openssl'
  'sqlite'
)
makedepends=('autoconf' 'git' 'python-sphinx')
checkdepends=('cppunit')
conflicts=('aria2' 'aria2-git')
provides=("aria2=${pkgver%%.r*}")
options=('staticlibs')
source=('git+https://github.com/tatsuhiro-t/aria2.git')
sha256sums=('SKIP')

pkgver() {
  cd aria2
  git describe --long --tags | \
    sed 's/\([^-]*-g\)/r\1/;s/-/./g;s/release.//'
}

prepare() {
  cd aria2
  autoreconf -i || autoreconf -i
}

build() {
  cd aria2
  ./configure \
    --prefix=/usr \
    --enable-static \
    --enable-shared \
    --enable-libaria2 \
    --without-gnutls \
    --with-openssl \
    --with-jemalloc \
    --with-libuv \
    --with-ca-bundle=/etc/ssl/certs/ca-certificates.crt \
    --with-bashcompletiondir=/usr/share/bash-completion/completions
  make
}

check() {
  cd aria2
  make check
}

package() {
  cd aria2
  make DESTDIR="$pkgdir" install
}
